FROM node:16-alpine3.15 as build-stage
RUN mkdir -p /app
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./dist /app/
# RUN npm run build --prod


FROM nginx:1.23.0-alpine
#Copy ci-dashboard-dist
COPY --from=build-stage /app/frontend/ /usr/share/nginx/html
#Copy default nginx configuration
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
