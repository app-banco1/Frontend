git:
	git pull
	git add .
	git commit -m "$m"
	git push

docker:
	ng build
	docker build . -t southamerica-west1-docker.pkg.dev/app-banco-358223/app-banco/frontend:latest -t southamerica-west1-docker.pkg.dev/app-banco-358223/app-banco/frontend:$v
	docker push southamerica-west1-docker.pkg.dev/app-banco-358223/app-banco/frontend:$v
	docker push southamerica-west1-docker.pkg.dev/app-banco-358223/app-banco/frontend:latest