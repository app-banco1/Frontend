import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Cuenta } from '../interfaces/cuenta.interface';
import { Movimiento } from '../interfaces/movimiento.interface';
import { ResponseAPI } from '../interfaces/responseAPI.interface';

@Injectable({
  providedIn: 'root'
})
export class MovimientosService {
  _urlBase = environment.baseUrl;

  constructor(
    private httpClient: HttpClient
  ){}

  public listar(): Observable<ResponseAPI> {
    return this.httpClient.get<ResponseAPI>(`${this._urlBase}/usuario/movimientos`);
  }

  public guardar(data: Movimiento): Observable<ResponseAPI> {
    return this.httpClient.post<ResponseAPI>(`${this._urlBase}/usuario/transferir`, data);
  }

  public saldo(): Observable<Cuenta>{
    return this.httpClient.get<Cuenta>(`${this._urlBase}/usuario/cuenta`);
  }
}
