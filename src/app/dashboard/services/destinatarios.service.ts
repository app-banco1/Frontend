import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Destinatario } from '../interfaces/destinatario.interface';
import { ResponseAPI } from '../interfaces/responseAPI.interface';

@Injectable({
  providedIn: 'root'
})
export class DestinatariosService {
  _urlBase = environment.baseUrl;

  constructor(
    private httpClient: HttpClient
  ) { }

  public listar(): Observable<ResponseAPI> {
    return this.httpClient.get<ResponseAPI>(`${this._urlBase}/destinatario/todos`);
  }

  public guardar(data: Destinatario): Observable<ResponseAPI> {
    return this.httpClient.post<ResponseAPI>(`${this._urlBase}/destinatario/crear`, data);
  }

  public buscarUno(id: number): Observable<ResponseAPI> {
    return this.httpClient.get<ResponseAPI>(`${this._urlBase}/destinatario/${id}`);
  }
}
