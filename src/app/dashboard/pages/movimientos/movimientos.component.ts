import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Movimiento } from '../../interfaces/movimiento.interface';
import { MovimientosService } from '../../services/movimientos.service';

@Component({
  selector: 'app-movimientos',
  templateUrl: './movimientos.component.html',
  styleUrls: []
})
export class MovimientosComponent implements OnInit {

  movimientos: Movimiento[] = [];
  status: number = 0;

  constructor(
    private movimientosService: MovimientosService,
    private toastrService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.cargarMovimientos();
  }

  cargarMovimientos(){
    this.movimientosService.listar().subscribe(
      data => {
        this.movimientos = data.object;
        this.status = data.statusCode;
      },
      err => {
        if(err.error.statusCode == 404){
          return;
        }
        this.toastrService.error(err.error.message, 'Error', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
      }
    )
  }


}
