import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Destinatario } from '../../interfaces/destinatario.interface';
import { DestinatariosService } from '../../services/destinatarios.service';

@Component({
  selector: 'app-destinatarios',
  templateUrl: './destinatarios.component.html',
  styleUrls: []
})
export class DestinatariosComponent implements OnInit {

  destinatarios: Destinatario[] = [];
  status: number = 0;

  constructor(
    private destinatarioService: DestinatariosService,
    private toastrService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.cargarDestinatarios();
  }

  cargarDestinatarios(){
    this.destinatarioService.listar().subscribe(
      data => {
        this.destinatarios = data.object;
        this.status = data.statusCode;
      },
      err => {
        if(err.error.statusCode == 404){
          return;
        }
        this.toastrService.error(err.error.message, 'Error', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
      }
    )
  }

}
