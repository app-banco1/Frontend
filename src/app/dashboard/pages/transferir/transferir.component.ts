import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MovimientosService } from '../../services/movimientos.service';
import { Movimiento } from '../../interfaces/movimiento.interface';
import { Cuenta } from '../../interfaces/cuenta.interface';

@Component({
  selector: 'app-transferir',
  templateUrl: './transferir.component.html',
  styleUrls: []
})
export class TransferirComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    monto: ['', [Validators.required]]
  });
  
  cuenta: Cuenta = {
    monto: 0,
    numero: 0
  };

  idContacto: number = 0;
  
  movimiento: Movimiento = {
    idDestinatario: 0,
    monto: 0,
    idDestinatario2:{
      nombre: ''
    }
  }

  get monto(){
    return this.miFormulario.get('monto');
  }

  constructor(
    private movimientoService: MovimientosService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private toastrService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.idContacto = this.activatedRoute.snapshot.params['id'];
    this.loadCuenta();
  }

  onSave(){
    const {monto} = this.miFormulario.value;
    this.movimiento = {
      idDestinatario: this.idContacto,
      monto,
      idDestinatario2:{
        nombre: ''
      }
    };

    this.movimientoService.guardar(this.movimiento).subscribe(
      data => {
        this.miFormulario.reset();
        this.loadCuenta();
        this.toastrService.success('Movimiento ingresado correctamente', 'Exito!', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        /* this.payload = this.tokenService.getPayload(); */
      },
      err => {
        this.toastrService.error(err.error.message, 'Error', {
          timeOut: 3000, positionClass: 'toast-top-center'
        })
      }
    );
  }

  loadCuenta(){
    this.movimientoService.saldo().subscribe(
      data => {
        this.cuenta = data;
      },
      err => {
        this.toastrService.error(err.error.message, 'Error', {
          timeOut: 3000, positionClass: 'toast-top-center'
        })
      }
    );
  }
}
