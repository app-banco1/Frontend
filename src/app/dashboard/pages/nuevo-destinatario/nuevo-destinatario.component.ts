import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Destinatario } from '../../interfaces/destinatario.interface';
import { DestinatariosService } from '../../services/destinatarios.service';

@Component({
  selector: 'app-nuevo-destinatario',
  templateUrl: './nuevo-destinatario.component.html',
  styleUrls: []
})
export class NuevoDestinatarioComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    correo: ['', [Validators.required, Validators.email]],
    nombre: ['', [Validators.required, Validators.minLength(4)]],
    cuenta: ['', [Validators.required]]
  });

  destinatario: Destinatario = {
    correo: '',
    nombre: '',
    cuenta: 0
  }

  get correo(){
    return this.miFormulario.get('correo');
  }

  get nombre(){
    return this.miFormulario.get('nombre');
  }

  get cuenta(){
    return this.miFormulario.get('cuenta');
  }

  constructor(
    private destinatarioService: DestinatariosService,
    private fb: FormBuilder,
    private toastrService: ToastrService,
  ) { }

  ngOnInit(): void {
  }

  onSave(){
    const {correo, nombre, cuenta} = this.miFormulario.value;
    this.destinatario = {
      correo,
      nombre,
      cuenta
    };

    this.destinatarioService.guardar(this.destinatario).subscribe(
      data => {
        this.miFormulario.reset();
        this.toastrService.success('Destinatario ingresado correctamente', 'Exito!', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
      },
      err => {
        this.toastrService.error(err.error.message, 'Error', {
          timeOut: 3000, positionClass: 'toast-top-center'
        })
      }
    );
  }

}
