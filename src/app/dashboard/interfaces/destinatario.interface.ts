export interface Destinatario{
    idDestinatario?: number
    nombre: string;
    correo: string;
    cuenta: number;
}
