import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './pages/main/main.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MovimientosComponent } from './pages/movimientos/movimientos.component';
import { DestinatariosComponent } from './pages/destinatarios/destinatarios.component';
import { TransferirComponent } from './pages/transferir/transferir.component';
import { NuevoDestinatarioComponent } from './pages/nuevo-destinatario/nuevo-destinatario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';

@NgModule({
  declarations: [
    MainComponent,
    MovimientosComponent,
    DestinatariosComponent,
    TransferirComponent,
    NuevoDestinatarioComponent,
    SidebarComponent,
    NavbarComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule
  ]
})
export class DashboardModule { }
