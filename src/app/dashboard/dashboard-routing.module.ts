import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DestinatariosComponent } from './pages/destinatarios/destinatarios.component';
import { MainComponent } from './pages/main/main.component';
import { MovimientosComponent } from './pages/movimientos/movimientos.component';
import { NuevoDestinatarioComponent } from './pages/nuevo-destinatario/nuevo-destinatario.component';
import { TransferirComponent } from './pages/transferir/transferir.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {path: '', component: DestinatariosComponent},
      {path: 'movimientos', component: MovimientosComponent},
      {path: 'nuevoDestinatario', component: NuevoDestinatarioComponent},
      {path: 'transferir/:id', component: TransferirComponent},
      {path: '**', redirectTo: ''}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
