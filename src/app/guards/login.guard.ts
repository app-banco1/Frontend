import { TokenService } from '../auth/services/token.service'; 
import { Injectable } from '@angular/core';
import { CanActivate, Router, CanLoad} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate, CanLoad{

  constructor(
    private tokenService: TokenService,
    private router: Router
  ) {}

  canActivate(): boolean {
    if (this.tokenService.isLogged()) {
      this.router.navigate(['/dashboard']);
      return false;
    }
    return true;
  }

  canLoad(): boolean {
    if (this.tokenService.isLogged()) {
      this.router.navigate(['/dashboard']);
      return false;
    }
    return true;
  }
}