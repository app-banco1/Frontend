import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LoginInterface } from '../../interfaces/login.interface';
import { AuthService } from '../../services/auth.service';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: []
})
export class LoginComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    correo: ['correo@correo.cl', [Validators.required, Validators.email]],
    password: ['pass', [Validators.required, Validators.minLength(4)]]
  });

  usuario: LoginInterface = {
    correo: '',
    password: ''
  }
  
  get correo(){
    return this.miFormulario.get('correo');
  }

  get password(){
    return this.miFormulario.get('password')
  }
  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    private toastrService: ToastrService,
    private router: Router,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
  }

  onLogin(){
    const {correo, password} = this.miFormulario.value;
    
    this.usuario = {
      correo,
      password
    };

    this.authService.login(this.usuario).subscribe(
      data => {
        if(data.statusCode == 200){
          this.tokenService.setToken(data.token);
          this.router.navigate(['/dashboard']);
        }
      },
      err => {
        this.toastrService.error(err.error.message, 'Error', {
          timeOut: 3000, positionClass: 'toast-top-center'
        })
      }
    );
  }

}
