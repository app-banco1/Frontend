import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { TokenService } from '../../services/token.service';
import { NuevoUsuarioInterface } from '../../interfaces/nuevo-usuario.interface';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: []
})
export class RegistroComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    correo: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4)]],
    username: ['', [Validators.required, Validators.minLength(4)]]
  });

  usuario: NuevoUsuarioInterface = {
    correo: '',
    password: '',
    username: ''
  }
  
  get correo(){
    return this.miFormulario.get('correo');
  }

  get password(){
    return this.miFormulario.get('password');
  }

  get username(){
    return this.miFormulario.get('username');
  }
  
  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    private toastrService: ToastrService,
    private router: Router,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
  }

  onRegister(){
    const {correo, password, username} = this.miFormulario.value;
    this.usuario = {
      correo,
      password,
      username
    };

    this.authService.registro(this.usuario).subscribe(
      data => {
        this.toastrService.success('Usuario ingresado correctamente', 'Exito!', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.tokenService.setToken(data.token);
        this.router.navigate(['/dashboard']);
      },
      err => {
        this.toastrService.error(err.error.message, 'Error', {
          timeOut: 3000, positionClass: 'toast-top-center'
        })
      }
    );
  }
}
