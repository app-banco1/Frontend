import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { LoginInterface } from "../interfaces/login.interface";
import { Observable } from "rxjs";
import { NuevoUsuarioInterface } from "../interfaces/nuevo-usuario.interface";

@Injectable({
  providedIn: 'root'
})
export class AuthService{
  _urlBase = environment.baseUrl;

  constructor(
    private httpClient: HttpClient
  ){}

  login(data: LoginInterface):Observable<any>{
    return this.httpClient.post<any>(this._urlBase + '/login', data);
  }

  registro(data: NuevoUsuarioInterface): Observable<any>{
    return this.httpClient.post<any>(this._urlBase+ '/usuario/crear', data);
  }
}