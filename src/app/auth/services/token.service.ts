import { Injectable } from "@angular/core";
import { PayloadInterface } from "../interfaces/payload.interface";

@Injectable({
  providedIn: 'root'
})
export class TokenService{
  payload: PayloadInterface | null = {
    correo: '',
    exp: 0,
    iat: 0,
    idCuenta: 0,
    idUsuario: 0,
    montoDisponible: 0,
    nombre: ''
  };
  
  constructor(){}

  isLogged(): boolean{
    if(this.getToken()){
      return true;
    }
    return false;
  }

  setToken(token:string): void{
    localStorage.setItem('token', token);
  }

  getToken(): string{
    const token = localStorage.getItem('token') || '';
    return token;
  }

  getPayload(): PayloadInterface | null{
    if(!this.isLogged()){
      return null;
    }
    const token = this.getToken();
    const payload = token.split('.')[1];
    const {correo, exp, iat, idCuenta, idUsuario, montoDisponible, nombre} = JSON.parse(atob(payload));
    this.payload ={
      correo,
      exp,
      iat,
      idCuenta,
      idUsuario,
      montoDisponible,
      nombre
    }
    return this.payload;
  }

  logout(): void{
    localStorage.clear();
  }
}