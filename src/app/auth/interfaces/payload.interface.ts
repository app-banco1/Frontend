export interface PayloadInterface{
    correo: string;
    exp: number;
    iat: number;
    idCuenta: number;
    idUsuario: number;
    montoDisponible: number;
    nombre: string;
}